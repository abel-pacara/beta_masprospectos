jQuery(document).ready(function (){
    var timeout = null;
    jQuery("#aff-select-dialog").dialog({modal: true, width: 500, open: function(){affiliate_search();}, autoOpen: false});

    jQuery("#change-affiliate-link").click(function (e){
        e.preventDefault();
        jQuery("#aff-select-dialog").dialog("open");
    });
    jQuery("#aff-param-input").keyup(function(e){
        clearTimeout(timeout);
        timeout = setTimeout(affiliate_search, 1*500);
    });
    jQuery("#aff-param-input").keydown(function (e){
        clearTimeout(timeout);
    });
    jQuery("aff-param-input").change(function (e){
        affiliate_search();
    })
});

function affiliate_search(){
    var search = jQuery("#aff-param-input").val();
    if(!search) return;
    var data = {
        'do'            :   'affiliate_search',
        'search'        :   search
    };
    jQuery("aff-select-loading").show();
    jQuery("#aff-select-dialog-resp").html("");
    jQuery.ajax({
        url         :   "ajax_cnt.php",
        cache       :   false,
        type        :   "POST",
        dataType    :   "json",
        data        :   data,
        success     :   function(resp, textStatus){
            jQuery("aff-select-loading").hide();
            if (resp.errorCode) {
                jQuery("#aff-select-dialog-resp").css("color", "red");
                jQuery("#aff-select-dialog-resp").html(resp.msg);
            } else {
                jQuery("#aff-select-dialog-resp").css("color", "black");
                jQuery("#aff-select-dialog-resp").html("<br/><table>");
                var i=0;
                for(key in resp.ret){
                    i++;
                    jQuery("#aff-select-dialog-resp").append(
                        "<tr><td>#"
                        +resp.ret[key].member_id+"</td><td>&nbsp;&nbsp;"+resp.ret[key].login+"</td><td>&nbsp;&nbsp;"
                        +resp.ret[key].name_f+"&nbsp;"+resp.ret[key].name_l+"</td><td>&nbsp;&nbsp;"
                        +resp.ret[key].email
                        +"&nbsp;<a style='color: blue' id='assign-affiliate-link' href='#' onClick='return change_affiliate("+resp.ret[key].member_id+",this)'>Assign</a>"
                        +"</td></tr>");
                }
                jQuery("#aff-select-dialog-resp").append("</table>");
            }
        }
     });
}
function change_affiliate(aff_id,l){
        jQuery("#aff-select-dialog").dialog("close");
        var data = {
            'do'        :   'change_affiliate',
            'member_id' :   member_id,
            'aff_id'    :   aff_id
        };
        jQuery.ajax({
           url      :   "ajax_cnt.php",
           cache    :   false,
           type     :   "POST",
           dataType :   "json",
           data     :   data,
           success  :   function(resp, textStatus){
                window.location.reload(false);
           }
        });
        return false;

}