<strong>aMail for GetResponse v3.1.9.0</strong>

You can see if there is a newer version of this plugin here:

	http://www.kencinnus.com/downloads/amail_getresponse/
      
The aMail Plugin will help you sync up your GetResponse lists with your 
aMember products and keep your members subscribed and unsubscribed automatically.

NOTE: Be sure the words on your membership site tell your members that they 
      are going to be subscribed to an email list in a prominent location 
      during the signup process.  
      
<strong>Instructions</strong>

These instructions assume you know how to create the lists you want for your 
products/members at GetResponse and have already done so.

 1. Set up some custom fields for your GetResponse list to receive member data from 
    aMember.
 
    a. Visit GetResponse and make sure the correct list is your current list.

    b. Click on the Contacts tab.  
    
    c. Click on the Add Custom Field Button.
    
       Add the custom fields that you need to sync up with aMember.

       You have to use the exact same names as shown below with the
       exception of your own additional aMember fields, then just use
       the exact same thing you entered in Field Name in aMember.
       
       Add these fields:
    
    	subscribeip &lt;-- Required.  You must add at least this one.

    	memberid    &lt;-- this one will always be sent

    	username    &lt;-- If you choose to send signup add these two
    	password
    	
    	street      &lt;-- If you choose to send address add these five
    	city
    	state
    	zip
    	country

    	phone       &lt;-- If you choose to send additional add them here
    	.
    	.
    	.
    	
    	
        NOTE: Use all lowercase letters for your custom field name.  Since
              GetResponse requires a default value you can just repeat the
              custom field name or enter a sensible default value.
        
    d. Repeat steps a-c for every list you are going to sync with aMember.
    
 2. Meanwhile, back on your own server in the aMember admin panel...
 
    Upload the GetResponse plugin files to the     <strong>amember/plugins/protect/amail_getresponse/</strong> 
    folder.

 3. Enable the aMail for GetResponse plugin at 
 
    <strong>aMember CP -&gt; Setup/Configuration -&gt; Plugins</strong>

 4. Configure the aMail for GetResponse plugin at 
 
    <strong>aMember CP -&gt; Setup/Configuration -&gt; aMail for GetResponse</strong>

    a. <strong>Test Mode?</strong> - 
       
       Write debug statements to the error log.

    b. <strong>CC Admin?</strong> -
       
       Check this box when you want an email send to your admin email address
       whenever an add/remove is sent to GetResponse.
       
    c. <strong>CC Guest?</strong> -
    
       Sometimes you want to alert someone else when members are subscribed/
       unsubscribed from GetResponse.  If so, you can enter their email
       address in this field and a copy of the email will go to them too.
       
    d. <strong>Do Not Unsubscribe?</strong> - 
       
       If you do not want this plugin to ever, under any circumstances send a
       remove request to GetResponse no matter what...then check this box.  
       
       This  defeats the purpose of using this plugin to keep GetResponse in 
       sync with your Newsletter unsubscribe flag and it may confuse your members.
       
       Subscribes still happen.
      
    e. <strong>Do Not Subscribe OR Unsubscribe?</strong> - 
       
       There may be times that you need to debug or experiment with something 
       and you want to leave the functionality of this plugin turned on but
       you do not want it to actually subscribe or unsubscribe your members
       at GetResponse while you play around with something.  You can check this 
       box and all of the adds/removes that would have normally gone to 
       GetResponse will now go to the admin email address instead, even if you 
       do not have the cc Admin field turned on.  
       
       When you are done you should think about using the aMember Rebuild DB 
       function if you want your changes you made while this was turned on to 
       actually take.  Since those changes did not happen then your aMember 
       database and the GetResponse lists may not be in sync.
       
    f. <strong>Do Database Rebuild?</strong> - 
    
       If this flag is unchecked then when you do the Rebuild DB command it
       will not do anything.
       
       If this flag is checked, then it will re-send all of the subscribe and
       unsubscribe information to GetResponse according the the members'
       current status.
       
       WARNING: If it re-subscribes someone who is already on the list at 
                GetResponse, it will start them over at message #1!

    g. <strong>Send Signup Information?</strong> -
       
       Check this box if you want the member's username and password to be 
       included in the subscribe email sent to GetResponse for their email 
       parser to read.  
       
       It can be useful if you want to make them confirm through GetResponse 
       before they get their userid or password that you have auto-generated 
       by aMember.  
       
       You can include the values in their first message from GetResponse.
       
       NOTE: Please realize these fields will never get updated in GetResponse again.
       
    h. <strong>Send Address Information?</strong> -
       
       Check this box if you want the member's street, city, state, zip and 
       country information included in the subscribe email sent to GetResponse for
       their email parser to read.

       NOTE: Please realize these fields will never get updated in GetResponse again.

    i. <strong>Send Additional Fields?</strong> -
       
       Whatever additional fields you set up in aMember that you also want to 
       send to GetResponse should be entered as a comma separated list here. 
       
       Use the internal field name, not the display name when entering them.
       
       For Example:
       
           1. You add an additional field in aMember called "phone."
           
           2. You enter "phone" into the Send Additional Fields box.
           
           3. You add a custom field in GetResponse called "phone" too.
           
       NOTE: Please realize these fields will never get updated in GetResponse again.

    j. <strong>Primary Default GetResponse Campaign</strong> - 
       
       Decide which campaign at GetResponse will be your primary default 
       campaign and enter it in the field provided.
      
       Every member will be added to this default member campaign when they 
       purchase any of your products UNLESS you configure products with their 
       own GetResponse campaign as described next.


    k. <strong>GetResponse API Key</strong> - 
    
       Enter your API key that GetResponse gives to you.
       
       To get your API key:
       
       a. Log into your GetResponse account.
       
       b. Click on the <strong>My Account</strong> link in the upper right corner.
       
       c. Click on the <strong>Use GetResponse API</strong> button in the middle of the page.
       
       d. Make sure it is enabled and then cut-and-paste the value from
          <strong>Your secret API key</strong> shown on the screen into
          the API key on the aMail for GetResponse plugin configuration page
          in aMember.
       
    l. Save aMail plugin settings.
    
    m. You will need to configure an email parser for this list at GetResponse as 
       described above.
       
 5. Update your /templates/signup.html page to always include the newsletter
    checkbox.  This is what allows GetResponse subscriptions or not for each member.
    
    NOTE: Nothing will work without this! It is also something you will have to
          do each time you upgrade aMember.
    
    a. Edit your /amember/templates/signup.html template file and locate these 
       lines:
    
       {if $newsletter_threads &gt; 0}
       &lt;tr&gt;
           &lt;th&gt;<strong>#_TPL_SIGNUP_NEWSLETTERS_SUBSCRIBE#</strong>&lt;br /&gt;
           &lt;div class="small"&gt;#_TPL_SIGNUP_NEWSLETTERS_SUBSCRIBE_1#&lt;/div&gt;
           &lt;/th&gt;
           &lt;td&gt;&lt;input type="checkbox" name="to_subscribe" value="1"
           {if $smarty.request.to_subscribe}checked="checked"{/if} /&gt;
           &lt;/td&gt;
       &lt;/tr&gt;
       {/if}
       
    b. Replace them with these:
    
       &lt;tr&gt;
         &lt;th&gt;
           &lt;strong&gt;#_TPL_SIGNUP_NEWSLETTERS_SUBSCRIBE#&lt;/strong&gt;&lt;br /&gt;
           &lt;div class="small"&gt;#_TPL_SIGNUP_NEWSLETTERS_SUBSCRIBE_1#&lt;/div&gt;
         &lt;/th&gt;&lt;td&gt;
           &lt;input type="checkbox" name="to_subscribe" value="1" checked="checked" /&gt;
           #_TPL_SIGNUP_NEWSLETTERS_SUBSCRIBE_2#
         &lt;/td&gt;
       &lt;/tr&gt;
       
       NOTE: You can edit your /amember/languages/en-custom.php and add this
             line to make it say anything you want after the checkbox:
             
       define ('_TPL_SIGNUP_NEWSLETTERS_SUBSCRIBE_2', 'Your password will be emailed to you.');

    c. Save your changes.
    
    d. Upload /templates/signup.html to your server.
       
    e. Use the /amember/languages/en-custom.php to change the words so they
       are suitable for your site.

 6. Update your /plugins/db/mysql/mysql.inc.php file to properly configure the
    unsubscribe flag based on the checkbox on the signup page.
    
    a. Edit your /plugins/db/mysql/mysql.inc.php file and locate these lines:
    
           function add_pending_user($vars){
               _amember_get_iconf_d();
               $REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
               global $member_additional_fields;
               $data = array();
       
               if (!strlen($vars['pass']))
                   $vars['pass'] = $vars['pass0'];
                   
    b. Right below them add these lines:
    
       // Begin Mod for aMail Plugin
       $vars['unsubscribed'] = (empty($vars['to_subscribe'])) ? 1 : 0;
       // End Mod for aMail Plugin

    c. Save your changes.
    
    d. Upload /plugins/db/mysql/mysql.inc.php to your server.

 7. Update your /templates/thanks.html page to tell the member to expect the 
    GetResponse confirmation.  Add this line at the top of the thanks.html template 
    file:
    
    NOTE: This is something you will have to do each time you upgrade aMember.
    
    a. Edit your /amember/templates/thanks.html template file and locate this 
       line:
    
       {include file="header.html"}
    
    b. Insert this line below:
       
       {include file="../plugins/protect/amail_getresponse/thanks.amail_getresponse.inc.html"}
       
    c. Save your changes.
    
    d. Upload /amember/templates/thanks.html to your server.
    
    NOTE: You can of course modify thanks.amail_getresponse.inc.html and thanks.html 
          with any message you choose.  It will be overwritten when you upgrade 
          this plugin.

    NOTE: If you also own the amThankYou, amOffers, amUpsell and/or amStrategy 
          or any other plugins then this is the order that they should appear 
          at the top of the thanks.html page:

       {include file="../plugins/protect/amupsell/thanks.amupsell.inc.html"}
       {include file="../plugins/protect/amoffers/thanks.amoffers.inc.html"}
       {include file="../plugins/protect/aminviter/thanks.aminviter.inc.html"}
       {include file="../plugins/protect/amstrategy/thanks.amstrategy.inc.html"}
       {include file="../plugins/protect/aminterests/thanks.aminterests.inc.html"}
       {include file="../plugins/protect/amail_getresponse/thanks.amail_getresponse.inc.html"}
       {include file="../plugins/protect/amthankyou/thanks.amproductlinks.inc.html"}
       
 8. RECOMMENDED: Update your member page so that when a member changes the 
    Newsletter Unsubscribe checkbox it keeps their GetResponse subscriptions in
    sync.
    
    a. Edit file /amember/member.php
    
    b. Find function 'update_subscriptions' and after...
    
          if (!$vars['unsubscribe']){

              $q = $db->query($s = "
                  UPDATE {$db->config['prefix']}members
                  SET unsubscribed=0
                  WHERE member_id=$member_id
              ");
              $db->add_member_threads($member_id, $vars['threads']);

          } else {

              $q = $db->query($s = "
               UPDATE {$db->config['prefix']}members
                  SET unsubscribed=1
                  WHERE member_id=$member_id
              ");

          }
    
       insert...
    
          //
          // Begin Mod for aMail Plugin
          //
          $newmember = $db->get_user($member_id);
          $oldmember = $newmember;
          $oldmember['unsubscribed'] = ($newmember['unsubscribed']) ? 0 : 1;
          plugin_subscription_updated($member_id,$oldmember,$newmember);
          //
          // End Mod for aMail Plugin
          //
    
    c. Save your changes.
    
    d. Upload /amember/member.php to your server.
    
    NOTE: The changes in steps 6, 7 and 8 will have to be re-done every time 
          you upgrade aMember.

 9. OPTIONAL: If you would like a particular product to subscribe members to a 
    particular GetResponse list that is different than your default member list then 
    you can follow these instructions.  

    a. Visit <strong>aMember CP -&gt; Manage Products -&gt; Edit</strong>.

    b. Enter the GetResponse list name into the Product's GetResponse Listname field.
    
    c. Save product settings.

    d. You will need to configure each list at GetResponse as described above.
       
    e. Repeat steps a-d for every product that has it's own list at GetResponse.
    
       NOTE: You can add multiple campaigns by separating them with commas.
             The member will get subscribed to all of the lists when they 
             purchase the product.  However, in most cases you do not have to 
             do this. It is far better to use the GetResponse automation rules to 
             add members to a second list when they subscribe to a first list.
             
       NOTE: If you set a product to "nolist" then when members purchase that
             product they will not be subscribed to any list, including the
             plugin default list.
    
10. OPTIONAL: Set up the thank you page for your GetResponse Confirmed Opt-In:

    a. Visit GetResponse and log into your account.
    
    b. Click on the Campaigns tab.

    c. Make sure the correct campaign is your current campaign.
    
    d. Click on the Settings link under the top row of tabs.
    
    e. Click on the Contact Settings tab in the middle of the page.
   
    f. Enter this into the Confirmation Page URL field:
   
       {$config.root_url}/plugins/protect/amail_getresponse/thanks.php
       
    g. Change anything else you want and click Save Settings at the bottom
       of the page.
       
    h. Repeat steps c-g for every GetResponse list you use on your site.
       
11. OPTIONAL: You might want to include the member's personal information in 
    the first followup email.
 
    This is great if you set aMember to generate passwords and your members 
    cannot get that until after they verify through GetResponse.
    
    a. Visit GetResponse and log into your account.
    
    b. Click on the Messages tab.

    b. When you create or manage messages include something like this:
    
        Here are your access details for for your membership -

	    Your User ID:   [[cus username]]  
	    Your Password:  [[cus password ]]
	    Your Member ID: [[cus memberid]]  
	
        Your personal details we have on record are -

            Name:        [[cus name]]
            Email:       [[cus email]]
            Address :    [[cus street]]
            City :       [[cus city]]
            State:       [[cus state]]
            Zip:         [[cus zip]]
            Country:     [[cus country]]
            Phone:       [[cus phone]]

            You signed up from the IP Address:  [[cus subscribeip]]
            
        You can log-on to your member pages at:
            
            {$config.root_url}/member.php
        
        On that page is a link to your Profile which you can use to change
        your password. 	
        
    g. Repeat steps a-b for every GetResponse list you sync with aMember.

12. Use the GetResponse automation rules to automatically subscribe/unsubscribe 
    members from one list to another list to suit your business rules.  This 
    can be handy for prospects and customers.  For example you could create two 
    lists in GetResponse, one for members and one for prospects.  You can create one
    free lifetime product in AMember and all of the rest of your products cost 
    the member money. You set the default aMail plugin values to the GetResponse 
    list you created for members and you leave the GetResponse fields blank on all 
    of your products that cost money but you fill in the fields for the free 
    product with the values for your GetResponse prospect list.  Customers who sign 
    up for your free product get put on your prospect list.  Customers who 
    purchase one of your other products get put on your members list.  In 
    GetResponse you can set up an automation rule that will take a person off of 
    your prospect list when they subscribe to your member list.
   
-------------------------------------------------------------------------------   
 
   Copyright (C) 2010 Ken Gary http://www.kencinnus.com/
                      All Rights Reserved
                    
   This file may not be distributed by anyone outside of Kencinnus, LLC except
   authorized contractors as specified.

   This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
   THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE.

   For aMail plugin support (to report bugs and request new features) visit:
   
       http://www.kencinnus.com/contact
                    
-------------------------------------------------------------------------------
