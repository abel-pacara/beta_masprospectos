            OGONE payment plugin configuration
           -----------------------------------------

CONFIGURATION
     
1. Enable and configure plugin at "aMember CP -> Setup/Configuration -> Plugins".

2. Configure the plugin at "aMember CP -> Setup/Configuration -> Ogone".

3. Make a test transaction

Note: ALIAS feature is requied for recurring billing.

-------------------------
What is a 'user for API'? 

An API user is a user specifically designed to be used by an application to make automatic
requests to the payment platform (automatic file upload/download, direct payment requests, queries etc.). 

The API user’s password does not have to be changed on a regular basis; this is more convenient
when the password has to be hard coded into your application. For security reasons, however,
we are unable to grant such users access to the administration module. 

The API user must be configured with the profile "Admin" in the User Management page. 

The following parameters are sent in the server request : 

PSPID: your PSPID
USERID: the API user
PSWD: the API user’s password. 

If you have lost the API user’s password, you can click on 'Send new password' under 'Users' in your account.
A mail will be sent to the user's email address with a new password, thereby invalidating any old password.

-------------------------
Security / IP address

For each request, Ogone system checks the IP address from which the request originates to ensure the
requests are being sent from the merchant's server. In the IP address field of the "Data and origin
verification" tab, checks for DirectLink section of the Technical Information page of your account you
must enter the IP address(es) or IP address range(s) of the servers that send your requests .
If the IP address from which the request originates has not been declared in the IP address field of the
"Data and origin verification" tab, checks for DirectLink section of the Technical Information page in
your account, you will receive the error message "unknown order/1/i". The IP address the request was
sent from will also be displayed in the error message.

-------------------------
A few credit card numbers have been defined in our test environment for testing purposes.
As these are only meant for testing, please refrain from using them in our production environment: 

Visa Brand : 4111 1111 1111 1111
Visa 3D Brand : 4000 0000 0000 0002
American Express Brand : 3741 1111 1111 111
MasterCard Brand : 5399 9999 9999 9999
Diners Brand : 3625 5695 5800 17
Bancontact/Mister Cash Brand : 67030000000000003 

Visa Purchasing Brand : 4484 1200 0000 0029
American Express Purchasing Brand : 3742 9101 9071 995

Any expiry date in a near future may be used and any 3 digit CVC code.
(4 digits for American Express and no CVC code for purchasing cards)
-------------------------
