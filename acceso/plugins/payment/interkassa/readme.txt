
***** IMPORTANT : THIS PLUGIN HAS NOT BEEN TESTED YET ****

1. Enable plugin in aMember CP -> Setup/Configuration -> Plugins

2. Configure it at aMember CP -> Setup/Configuration -> Interkassa

3. Configure following URLs in your Interkassa account:

   Status URL: {$config.root_url}/plugins/payment/interkassa/ipn.php
   Success URL: {$config.root_url}/plugins/payment/interkassa/thanks.php
   Fail URL: {$config.root_url}/cancel.php

4. Run test transaction to test the integration
