                      Netbilling_form plugin installation

 1. Enable plugin: go to aMember CP -> Setup/Configuration -> Plugins and enable
	"netbilling_form" payment plugin.
 2. Configure plugin: go to aMember CP -> Setup/Configuration -> Netbilling_form
	and configure it.
 3. Configure Return URL in your Netbilling account  to this URL:
    {$config.root_url}/plugins/payment/netbilling_form/thanks.php
 4. Configure Postback CGI URL in your Netbilling account  to this URL:
    {$config.root_url}/plugins/payment/netbilling_form/ipn.php
 5. Run a test transaction to ensure everything is working correctly.

