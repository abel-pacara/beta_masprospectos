www.profit-bill.com plugin instalaltion

1) copy all plugin files to the folder /amember/plugin/payment/profitbill

2) enable plugin from AmemberCP->Setup/Configuration->Plugins

3) visit AmemberCP->Setup/Configuration->www.profit-bill.com
   and follow instruction

4) add desired projects from http://profit-bill.com/projects/smsapi.html

5) please use url {$config.root_url}/plugins/payment/profitbill/ipn.php
   for the dynamic handler of your projects

6) set up next options 'Profitbill Project ID','Profitbill Project Payments Count', 
   'Profitbill Project Secret Code' for necessary products in Amember

7) if needs please edit language constatnts in file 
   /amember/plugins/payment/profitbill/form.html