Premier Payment System
======================

1. Unzip and upload files to /amember/plugins/payment/premier_ps folder

2. Enable and configure plugin in aMember CP -> Setup/Configuration

3. You NEED to use external cron with this plugin
    (See aMember CP -> Setup/Configuration -> Advanced)


4. Testing Information

     Transaction Testing Account
             Transactions can be tested using one of two methods. First, transactions
             can be submitted to any merchant account that is in test mode. Keep in
             mind that if an account is in test mode, all valid credit cards will be
             approved but no charges will actually be processed.
             The Payment Gateway demo account can also be used for testing at any
             time. Please use the following username and password for testing with
             this account:
               Username:       demo
               Password:       password

     Test Transaction Information
             Test transactions can be submitted with the following information:
               Visa                                          4111111111111111
                                                             5431111111111111
               MasterCard
                                                             6011601160116611
               DiscoverCard
                                                              341111111111111
               American Express
               Credit Card Expiration:                                      10/10
               Amount                                                      > 1.00

Triggering Errors in Test Mode
     To cause a declined message, pass an amount less than 1.00.
     To trigger a fatal error message, pass an invalid card number.
     To simulate an AVS Match, pass 888 in the address1 field, 77777 for zip.
     To simulate a CVV Match, pass 999 in the cvv field.

