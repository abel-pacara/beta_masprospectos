    
         Pay.nl plugin installation

 1. Enable plugin: go to aMember CP -> Setup -> Plugins and enable 
    "paynl" payment plugin.
 2. Configure plugin: go to aMember CP -> Setup -> Pay.nl 
    and configure it.
 3. Configure your Pay.nl Account - contact Pay.nl support and ask
    them to set:
    IPN URL to
    {$config.root_url}/plugins/payment/paynl/ipn.php
 4. Run a test transaction to ensure everthing is working correctly. 

 
