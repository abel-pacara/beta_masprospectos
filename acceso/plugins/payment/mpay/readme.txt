1. Enable and configure plugin in aMember Control Panel

2. Enable test mode and try it:

 TEST CARD NUMBER | CARD TYPE
---------------------------------------
4444111122223333       Visa

-----------------------------------------------------------------------------

Thank you for contacting our customer service group.
Please let us know if there is anything we can do to help you in the
future.