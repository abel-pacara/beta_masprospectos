<?php
//Include Common Files @1-AAEF2F9C
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "cfg_adicionales_list.php");
include_once(RelativePath . "/Common.php");
include_once(RelativePath . "/Template.php");
include_once(RelativePath . "/Sorter.php");
include_once(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @3-373E582D
include_once(RelativePath . "/./footer.php");
//End Include Page implementation

//Include Page implementation @6-52371612
include_once(RelativePath . "/./header.php");
//End Include Page implementation

class clsGridusuarios_adicionales_usua1 { //usuarios_adicionales_usua1 class @19-BBAD856F

//Variables @19-F27BCE1E

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_fecha;
    public $Sorter_usuario_id;
    public $Sorter_nombre;
    public $Sorter_serv_hosting;
    public $Sorter_serv_responder;
    public $Sorter_estado;
//End Variables

//Class_Initialize Event @19-41C07BF3
    function clsGridusuarios_adicionales_usua1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "usuarios_adicionales_usua1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid usuarios_adicionales_usua1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsusuarios_adicionales_usua1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 25;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("usuarios_adicionales_usua1Order", "");
        $this->SorterDirection = CCGetParam("usuarios_adicionales_usua1Dir", "");

        $this->fecha = new clsControl(ccsLabel, "fecha", "fecha", ccsDate, $DefaultDateFormat, CCGetRequestParam("fecha", ccsGet, NULL), $this);
        $this->nombre = new clsControl(ccsLabel, "nombre", "nombre", ccsText, "", CCGetRequestParam("nombre", ccsGet, NULL), $this);
        $this->serv_hosting = new clsControl(ccsLabel, "serv_hosting", "serv_hosting", ccsBoolean, array("Si", "No", ""), CCGetRequestParam("serv_hosting", ccsGet, NULL), $this);
        $this->serv_responder = new clsControl(ccsLabel, "serv_responder", "serv_responder", ccsBoolean, array("Si", "No", ""), CCGetRequestParam("serv_responder", ccsGet, NULL), $this);
        $this->comentarios = new clsControl(ccsLabel, "comentarios", "comentarios", ccsMemo, "", CCGetRequestParam("comentarios", ccsGet, NULL), $this);
        $this->anotaciones = new clsControl(ccsLabel, "anotaciones", "anotaciones", ccsMemo, "", CCGetRequestParam("anotaciones", ccsGet, NULL), $this);
        $this->estado = new clsControl(ccsLabel, "estado", "estado", ccsText, "", CCGetRequestParam("estado", ccsGet, NULL), $this);
        $this->apellido = new clsControl(ccsLabel, "apellido", "apellido", ccsText, "", CCGetRequestParam("apellido", ccsGet, NULL), $this);
        $this->email = new clsControl(ccsLabel, "email", "email", ccsText, "", CCGetRequestParam("email", ccsGet, NULL), $this);
        $this->dominio = new clsControl(ccsLabel, "dominio", "dominio", ccsText, "", CCGetRequestParam("dominio", ccsGet, NULL), $this);
        $this->dominio->HTML = true;
        $this->nom_nivel = new clsControl(ccsLabel, "nom_nivel", "nom_nivel", ccsText, "", CCGetRequestParam("nom_nivel", ccsGet, NULL), $this);
        $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", ccsGet, NULL), $this);
        $this->Link1->Page = "cfg_adicionales_activar.php";
        $this->claves = new clsControl(ccsLabel, "claves", "claves", ccsText, "", CCGetRequestParam("claves", ccsGet, NULL), $this);
        $this->ImageLink1 = new clsControl(ccsImageLink, "ImageLink1", "ImageLink1", ccsText, "", CCGetRequestParam("ImageLink1", ccsGet, NULL), $this);
        $this->ImageLink1->Page = "cfg_detalle_pagos.php";
        $this->ImageLink2 = new clsControl(ccsImageLink, "ImageLink2", "ImageLink2", ccsText, "", CCGetRequestParam("ImageLink2", ccsGet, NULL), $this);
        $this->ImageLink2->Page = "cfg_usuarios_info.php";
        $this->usuario_id = new clsControl(ccsLabel, "usuario_id", "usuario_id", ccsInteger, "", CCGetRequestParam("usuario_id", ccsGet, NULL), $this);
        $this->Link2 = new clsControl(ccsLink, "Link2", "Link2", ccsText, "", CCGetRequestParam("Link2", ccsGet, NULL), $this);
        $this->Link2->Page = "cfg_adicionales_editar.php";
        $this->Sorter_fecha = new clsSorter($this->ComponentName, "Sorter_fecha", $FileName, $this);
        $this->Sorter_usuario_id = new clsSorter($this->ComponentName, "Sorter_usuario_id", $FileName, $this);
        $this->Sorter_nombre = new clsSorter($this->ComponentName, "Sorter_nombre", $FileName, $this);
        $this->Sorter_serv_hosting = new clsSorter($this->ComponentName, "Sorter_serv_hosting", $FileName, $this);
        $this->Sorter_serv_responder = new clsSorter($this->ComponentName, "Sorter_serv_responder", $FileName, $this);
        $this->Sorter_estado = new clsSorter($this->ComponentName, "Sorter_estado", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
        $this->Navigator->PageSizes = array("1", "5", "10", "25", "50");
    }
//End Class_Initialize Event

//Initialize Method @19-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @19-A2C9BA73
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_usuario_id"] = CCGetFromGet("s_usuario_id", NULL);
        $this->DataSource->Parameters["urls_email"] = CCGetFromGet("s_email", NULL);
        $this->DataSource->Parameters["urls_estado"] = CCGetFromGet("s_estado", NULL);
        $this->DataSource->Parameters["urls_dominio"] = CCGetFromGet("s_dominio", NULL);
        $this->DataSource->Parameters["urls_serv_hosting"] = CCGetFromGet("s_serv_hosting", NULL);
        $this->DataSource->Parameters["urls_serv_responder"] = CCGetFromGet("s_serv_responder", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["fecha"] = $this->fecha->Visible;
            $this->ControlsVisible["nombre"] = $this->nombre->Visible;
            $this->ControlsVisible["serv_hosting"] = $this->serv_hosting->Visible;
            $this->ControlsVisible["serv_responder"] = $this->serv_responder->Visible;
            $this->ControlsVisible["comentarios"] = $this->comentarios->Visible;
            $this->ControlsVisible["anotaciones"] = $this->anotaciones->Visible;
            $this->ControlsVisible["estado"] = $this->estado->Visible;
            $this->ControlsVisible["apellido"] = $this->apellido->Visible;
            $this->ControlsVisible["email"] = $this->email->Visible;
            $this->ControlsVisible["dominio"] = $this->dominio->Visible;
            $this->ControlsVisible["nom_nivel"] = $this->nom_nivel->Visible;
            $this->ControlsVisible["Link1"] = $this->Link1->Visible;
            $this->ControlsVisible["claves"] = $this->claves->Visible;
            $this->ControlsVisible["ImageLink1"] = $this->ImageLink1->Visible;
            $this->ControlsVisible["ImageLink2"] = $this->ImageLink2->Visible;
            $this->ControlsVisible["usuario_id"] = $this->usuario_id->Visible;
            $this->ControlsVisible["Link2"] = $this->Link2->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->fecha->SetValue($this->DataSource->fecha->GetValue());
                $this->nombre->SetValue($this->DataSource->nombre->GetValue());
                $this->serv_hosting->SetValue($this->DataSource->serv_hosting->GetValue());
                $this->serv_responder->SetValue($this->DataSource->serv_responder->GetValue());
                $this->comentarios->SetValue($this->DataSource->comentarios->GetValue());
                $this->anotaciones->SetValue($this->DataSource->anotaciones->GetValue());
                $this->estado->SetValue($this->DataSource->estado->GetValue());
                $this->apellido->SetValue($this->DataSource->apellido->GetValue());
                $this->email->SetValue($this->DataSource->email->GetValue());
                $this->dominio->SetValue($this->DataSource->dominio->GetValue());
                $this->nom_nivel->SetValue($this->DataSource->nom_nivel->GetValue());
                $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->Link1->Parameters = CCAddParam($this->Link1->Parameters, "adicionales_id", $this->DataSource->f("adicionales_id"));
                $this->Link1->Parameters = CCAddParam($this->Link1->Parameters, "usuario_id", $this->DataSource->f("usuario_id"));
                $this->claves->SetValue($this->DataSource->claves->GetValue());
                $this->ImageLink1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->ImageLink1->Parameters = CCAddParam($this->ImageLink1->Parameters, "usuario_id", $this->DataSource->f("usuario_id"));
                $this->ImageLink2->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->ImageLink2->Parameters = CCAddParam($this->ImageLink2->Parameters, "usuario_id", $this->DataSource->f("usuario_id"));
                $this->usuario_id->SetValue($this->DataSource->usuario_id->GetValue());
                $this->Link2->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->Link2->Parameters = CCAddParam($this->Link2->Parameters, "adicionales_id", $this->DataSource->f("adicionales_id"));
                $this->Link2->Parameters = CCAddParam($this->Link2->Parameters, "usuario_id", $this->DataSource->f("usuario_id"));
                $this->usuario_id->Attributes->SetValue("nom_usuario", $this->DataSource->f("nom_usuario"));
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->fecha->Show();
                $this->nombre->Show();
                $this->serv_hosting->Show();
                $this->serv_responder->Show();
                $this->comentarios->Show();
                $this->anotaciones->Show();
                $this->estado->Show();
                $this->apellido->Show();
                $this->email->Show();
                $this->dominio->Show();
                $this->nom_nivel->Show();
                $this->Link1->Show();
                $this->claves->Show();
                $this->ImageLink1->Show();
                $this->ImageLink2->Show();
                $this->usuario_id->Show();
                $this->Link2->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        $this->Navigator->PageSize = $this->PageSize;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        if ($this->Navigator->TotalPages <= 1) {
            $this->Navigator->Visible = false;
        }
        $this->Sorter_fecha->Show();
        $this->Sorter_usuario_id->Show();
        $this->Sorter_nombre->Show();
        $this->Sorter_serv_hosting->Show();
        $this->Sorter_serv_responder->Show();
        $this->Sorter_estado->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @19-0CB1F646
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->fecha->Errors->ToString());
        $errors = ComposeStrings($errors, $this->nombre->Errors->ToString());
        $errors = ComposeStrings($errors, $this->serv_hosting->Errors->ToString());
        $errors = ComposeStrings($errors, $this->serv_responder->Errors->ToString());
        $errors = ComposeStrings($errors, $this->comentarios->Errors->ToString());
        $errors = ComposeStrings($errors, $this->anotaciones->Errors->ToString());
        $errors = ComposeStrings($errors, $this->estado->Errors->ToString());
        $errors = ComposeStrings($errors, $this->apellido->Errors->ToString());
        $errors = ComposeStrings($errors, $this->email->Errors->ToString());
        $errors = ComposeStrings($errors, $this->dominio->Errors->ToString());
        $errors = ComposeStrings($errors, $this->nom_nivel->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Link1->Errors->ToString());
        $errors = ComposeStrings($errors, $this->claves->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ImageLink1->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ImageLink2->Errors->ToString());
        $errors = ComposeStrings($errors, $this->usuario_id->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Link2->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End usuarios_adicionales_usua1 Class @19-FCB6E20C

class clsusuarios_adicionales_usua1DataSource extends clsDBDatos {  //usuarios_adicionales_usua1DataSource Class @19-8D3928E2

//DataSource Variables @19-E45690FC
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $fecha;
    public $nombre;
    public $serv_hosting;
    public $serv_responder;
    public $comentarios;
    public $anotaciones;
    public $estado;
    public $apellido;
    public $email;
    public $dominio;
    public $nom_nivel;
    public $claves;
    public $usuario_id;
//End DataSource Variables

//DataSourceClass_Initialize Event @19-BCAB9B9B
    function clsusuarios_adicionales_usua1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid usuarios_adicionales_usua1";
        $this->Initialize();
        $this->fecha = new clsField("fecha", ccsDate, $this->DateFormat);
        
        $this->nombre = new clsField("nombre", ccsText, "");
        
        $this->serv_hosting = new clsField("serv_hosting", ccsBoolean, $this->BooleanFormat);
        
        $this->serv_responder = new clsField("serv_responder", ccsBoolean, $this->BooleanFormat);
        
        $this->comentarios = new clsField("comentarios", ccsMemo, "");
        
        $this->anotaciones = new clsField("anotaciones", ccsMemo, "");
        
        $this->estado = new clsField("estado", ccsText, "");
        
        $this->apellido = new clsField("apellido", ccsText, "");
        
        $this->email = new clsField("email", ccsText, "");
        
        $this->dominio = new clsField("dominio", ccsText, "");
        
        $this->nom_nivel = new clsField("nom_nivel", ccsText, "");
        
        $this->claves = new clsField("claves", ccsText, "");
        
        $this->usuario_id = new clsField("usuario_id", ccsInteger, "");
        

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @19-0B5AE920
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "adicionales.adicionales_id";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_fecha" => array("fecha", ""), 
            "Sorter_usuario_id" => array("adicionales.usuario_id", ""), 
            "Sorter_nombre" => array("nombre, apellido", ""), 
            "Sorter_serv_hosting" => array("serv_hosting", ""), 
            "Sorter_serv_responder" => array("serv_responder", ""), 
            "Sorter_estado" => array("estado", "")));
    }
//End SetOrder Method

//Prepare Method @19-890148BB
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_usuario_id", ccsInteger, "", "", $this->Parameters["urls_usuario_id"], "", false);
        $this->wp->AddParameter("2", "urls_email", ccsText, "", "", $this->Parameters["urls_email"], "", false);
        $this->wp->AddParameter("3", "urls_estado", ccsText, "", "", $this->Parameters["urls_estado"], "", false);
        $this->wp->AddParameter("4", "urls_dominio", ccsText, "", "", $this->Parameters["urls_dominio"], "", false);
        $this->wp->AddParameter("5", "urls_serv_hosting", ccsText, "", "", $this->Parameters["urls_serv_hosting"], "", false);
        $this->wp->AddParameter("6", "urls_serv_responder", ccsText, "", "", $this->Parameters["urls_serv_responder"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "adicionales.usuario_id", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "usuarios.email", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "adicionales.estado", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "adicionales.dominio", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->wp->Criterion[5] = $this->wp->Operation(opContains, "adicionales.serv_hosting", $this->wp->GetDBValue("5"), $this->ToSQL($this->wp->GetDBValue("5"), ccsText),false);
        $this->wp->Criterion[6] = $this->wp->Operation(opContains, "adicionales.serv_responder", $this->wp->GetDBValue("6"), $this->ToSQL($this->wp->GetDBValue("6"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]), 
             $this->wp->Criterion[6]);
    }
//End Prepare Method

//Open Method @19-3E9E3751
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM (usuarios RIGHT JOIN adicionales ON\n\n" .
        "adicionales.usuario_id = usuarios.usuario_id) LEFT JOIN niveles ON\n\n" .
        "usuarios.nivel_id = niveles.nivel_id";
        $this->SQL = "SELECT adicionales.*, nombre, apellido, email, nom_nivel, public_skype, concat(',<br />', dominio) AS nom_dominio, concat(nombre, ' ', apellido) AS nom_usuario \n\n" .
        "FROM (usuarios RIGHT JOIN adicionales ON\n\n" .
        "adicionales.usuario_id = usuarios.usuario_id) LEFT JOIN niveles ON\n\n" .
        "usuarios.nivel_id = niveles.nivel_id {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @19-A358BAF9
    function SetValues()
    {
        $this->fecha->SetDBValue(trim($this->f("fecha")));
        $this->nombre->SetDBValue($this->f("nombre"));
        $this->serv_hosting->SetDBValue(trim($this->f("serv_hosting")));
        $this->serv_responder->SetDBValue(trim($this->f("serv_responder")));
        $this->comentarios->SetDBValue($this->f("comentarios"));
        $this->anotaciones->SetDBValue($this->f("anotaciones"));
        $this->estado->SetDBValue($this->f("estado"));
        $this->apellido->SetDBValue($this->f("apellido"));
        $this->email->SetDBValue($this->f("email"));
        $this->dominio->SetDBValue($this->f("nom_dominio"));
        $this->nom_nivel->SetDBValue($this->f("nom_nivel"));
        $this->claves->SetDBValue($this->f("claves"));
        $this->usuario_id->SetDBValue(trim($this->f("usuario_id")));
    }
//End SetValues Method

} //End usuarios_adicionales_usua1DataSource Class @19-FCB6E20C

class clsRecordusuarios_adicionales_usua { //usuarios_adicionales_usua Class @27-D614A579

//Variables @27-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @27-9F39D664
    function clsRecordusuarios_adicionales_usua($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record usuarios_adicionales_usua/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "usuarios_adicionales_usua";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = explode(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_usuario_id = new clsControl(ccsTextBox, "s_usuario_id", "s_usuario_id", ccsInteger, "", CCGetRequestParam("s_usuario_id", $Method, NULL), $this);
            $this->s_serv_hosting = new clsControl(ccsListBox, "s_serv_hosting", "s_serv_hosting", ccsText, "", CCGetRequestParam("s_serv_hosting", $Method, NULL), $this);
            $this->s_serv_hosting->DSType = dsListOfValues;
            $this->s_serv_hosting->Values = array(array("V", "Con Hosting"), array("F", "Sin Hosting"));
            $this->s_email = new clsControl(ccsTextBox, "s_email", "s_email", ccsText, "", CCGetRequestParam("s_email", $Method, NULL), $this);
            $this->s_estado = new clsControl(ccsListBox, "s_estado", "s_estado", ccsText, "", CCGetRequestParam("s_estado", $Method, NULL), $this);
            $this->s_estado->DSType = dsListOfValues;
            $this->s_estado->Values = array(array("Pendiente", "Pendiente"), array("Rechazado", "Rechazado"), array("Activado", "Activado"), array("Desactivado", "Desactivado"));
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
            $this->s_dominio = new clsControl(ccsTextBox, "s_dominio", "s_dominio", ccsText, "", CCGetRequestParam("s_dominio", $Method, NULL), $this);
            $this->s_serv_responder = new clsControl(ccsListBox, "s_serv_responder", "s_serv_responder", ccsText, "", CCGetRequestParam("s_serv_responder", $Method, NULL), $this);
            $this->s_serv_responder->DSType = dsListOfValues;
            $this->s_serv_responder->Values = array(array("V", "Con Autoresponder"), array("F", "Sin Autoresponder"));
        }
    }
//End Class_Initialize Event

//Validate Method @27-43BC08A7
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_usuario_id->Validate() && $Validation);
        $Validation = ($this->s_serv_hosting->Validate() && $Validation);
        $Validation = ($this->s_email->Validate() && $Validation);
        $Validation = ($this->s_estado->Validate() && $Validation);
        $Validation = ($this->s_dominio->Validate() && $Validation);
        $Validation = ($this->s_serv_responder->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_usuario_id->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_serv_hosting->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_email->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_estado->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_dominio->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_serv_responder->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @27-5C4B1384
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_usuario_id->Errors->Count());
        $errors = ($errors || $this->s_serv_hosting->Errors->Count());
        $errors = ($errors || $this->s_email->Errors->Count());
        $errors = ($errors || $this->s_estado->Errors->Count());
        $errors = ($errors || $this->s_dominio->Errors->Count());
        $errors = ($errors || $this->s_serv_responder->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//MasterDetail @27-ED598703
function SetPrimaryKeys($keyArray)
{
    $this->PrimaryKeys = $keyArray;
}
function GetPrimaryKeys()
{
    return $this->PrimaryKeys;
}
function GetPrimaryKey($keyName)
{
    return $this->PrimaryKeys[$keyName];
}
//End MasterDetail

//Operation Method @27-C10D4BCB
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "cfg_adicionales_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "cfg_adicionales_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @27-9CC29EF5
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_serv_hosting->Prepare();
        $this->s_estado->Prepare();
        $this->s_serv_responder->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_usuario_id->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_serv_hosting->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_email->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_estado->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_dominio->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_serv_responder->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_usuario_id->Show();
        $this->s_serv_hosting->Show();
        $this->s_email->Show();
        $this->s_estado->Show();
        $this->Button_DoSearch->Show();
        $this->s_dominio->Show();
        $this->s_serv_responder->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End usuarios_adicionales_usua Class @27-FCB6E20C

//Initialize Page @1-145754C7
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "cfg_adicionales_list.html";
$BlockToParse = "main";
$TemplateEncoding = "ISO-8859-1";
$ContentType = "text/html";
$PathToRoot = "./";
$Charset = $Charset ? $Charset : "iso-8859-1";
//End Initialize Page

//Authenticate User @1-D4E93EF5
CCSecurityRedirect("3;4;5;6;7", "./error_nivel.php");
//End Authenticate User

//Include events file @1-1939C95F
include_once("./cfg_adicionales_list_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-E0768A3B
$DBDatos = new clsDBDatos();
$MainPage->Connections["Datos"] = & $DBDatos;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$footer = new clsfooter("./", "footer", $MainPage);
$footer->Initialize();
$header = new clsheader("./", "header", $MainPage);
$header->Initialize();
$page_title = new clsControl(ccsLabel, "page_title", "page_title", ccsText, "", CCGetRequestParam("page_title", ccsGet, NULL), $MainPage);
$page_description = new clsControl(ccsLabel, "page_description", "page_description", ccsText, "", CCGetRequestParam("page_description", ccsGet, NULL), $MainPage);
$page_keywords = new clsControl(ccsLabel, "page_keywords", "page_keywords", ccsText, "", CCGetRequestParam("page_keywords", ccsGet, NULL), $MainPage);
$usuarios_adicionales_usua1 = new clsGridusuarios_adicionales_usua1("", $MainPage);
$usuarios_adicionales_usua = new clsRecordusuarios_adicionales_usua("", $MainPage);
$MainPage->footer = & $footer;
$MainPage->header = & $header;
$MainPage->page_title = & $page_title;
$MainPage->page_description = & $page_description;
$MainPage->page_keywords = & $page_keywords;
$MainPage->usuarios_adicionales_usua1 = & $usuarios_adicionales_usua1;
$MainPage->usuarios_adicionales_usua = & $usuarios_adicionales_usua;
if(!is_array($page_title->Value) && !strlen($page_title->Value) && $page_title->Value !== false)
    $page_title->SetText(PAGE_TITLE);
if(!is_array($page_description->Value) && !strlen($page_description->Value) && $page_description->Value !== false)
    $page_description->SetText(PAGE_DESCRIPTION);
if(!is_array($page_keywords->Value) && !strlen($page_keywords->Value) && $page_keywords->Value !== false)
    $page_keywords->SetText(PAGE_KEYWORDS);
$usuarios_adicionales_usua1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

if ($Charset) {
    header("Content-Type: " . $ContentType . "; charset=" . $Charset);
} else {
    header("Content-Type: " . $ContentType);
}
//End Initialize Objects

//Initialize HTML Template @1-6DB8AD0B
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "ISO-8859-1");
$Tpl->block_path = "/$BlockToParse";
$Attributes->SetValue("FileName", FileName);
$Attributes->SetValue("uniqid", uniqid());
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->SetValue("pathToRoot", "");
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-8461E872
$footer->Operations();
$header->Operations();
$usuarios_adicionales_usua->Operation();
//End Execute Components

//Go to destination page @1-13B91622
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBDatos->close();
    header("Location: " . $Redirect);
    $footer->Class_Terminate();
    unset($footer);
    $header->Class_Terminate();
    unset($header);
    unset($usuarios_adicionales_usua1);
    unset($usuarios_adicionales_usua);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-23182C3A
$footer->Show();
$header->Show();
$usuarios_adicionales_usua1->Show();
$usuarios_adicionales_usua->Show();
$page_title->Show();
$page_description->Show();
$page_keywords->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
if (!isset($main_block)) $main_block = $Tpl->GetVar($BlockToParse);
$main_block = CCConvertEncoding($main_block, $FileEncoding, $TemplateEncoding);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-402EEF8B
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBDatos->close();
$footer->Class_Terminate();
unset($footer);
$header->Class_Terminate();
unset($header);
unset($usuarios_adicionales_usua1);
unset($usuarios_adicionales_usua);
unset($Tpl);
//End Unload Page


?>
