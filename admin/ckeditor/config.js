/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	//config.uiColor = '#CCC';
    config.skin = 'office2003' ;
    config.enterMode = CKEDITOR.ENTER_BR;   
    config.toolbar = 'Normal';
	config.extraPlugins='flvPlayer,inserthtml';


    config.toolbar_Full =
    [
        ['Source','-','Preview','-','Templates'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
        '/',
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Link','Unlink','Anchor'],
        ['Image','Flash','flvPlayer','inserthtml','Table','HorizontalRule','Smiley','SpecialChar'],
        '/',
        ['Styles','Format','Font','FontSize'],
        ['TextColor','BGColor'],
        ['Maximize', 'ShowBlocks','-','About']
    ];

    config.toolbar_Basic =
    [
        ['Bold', 'Italic', 'Underline', 'Strike', 'FontSize', 'TextColor','BGColor', '-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' , '-', 'Image', 'Link', 'Unlink', 'inserthtml', '-','Source']
    ];

    config.toolbar_Normal =
    [
        ['Source','-','Preview','-','Templates'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','NumberedList','BulletedList'],
        ['Link','Unlink','Anchor'],
        ['Table','Image','Flash','inserthtml','HorizontalRule','Smiley','SpecialChar'],
        ['Styles','Format','Font','FontSize'],
        ['TextColor','BGColor','Maximize']
    ];
};