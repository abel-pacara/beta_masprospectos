CKEDITOR.plugins.setLang('flvPlayer', 'en', {
    flvPlayer: {
        flvPlayer: 'Video Player',
        btnLabel: 'Video Player',
        label: 'Insertar/Editar video',
        title: 'Insertar/Editar Video (flv, mp4)',
        notEmpty: 'Por favor inserte la ruta completa para el archivo de video (.flv o .mp4)',
        upload : 'Cargar',
        infoTab : 'Info',
        width : 'Ancho',
        height : 'Alto',
        border : 'Borde',
        hSpace : 'Espacio Hor.',
        vSpace : 'Espacio Vert.',
        align : 'Alinear',
        alignLeft : 'Izquierda',
        alignRight : 'Deracha',
        btnUpload : 'Cargar',
        allowFullscreen : 'Permitir pantalla completa',
        autoplay : 'AutoIniciar'
    }
});