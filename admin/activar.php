<?php
//Include Common Files @1-C187A9F7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "activar.php");

include_once(RelativePath . "/Common.php");

include_once(RelativePath . "/Template.php");
include_once(RelativePath . "/Sorter.php");
include_once(RelativePath . "/Navigator.php");
//End Include Common Files


//Initialize Page @1-C25D0D92
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "activar.html";
$BlockToParse = "main";
$TemplateEncoding = "ISO-8859-1";
$ContentType = "text/html";
$PathToRoot = "./";
$Charset = $Charset ? $Charset : "iso-8859-1";
//End Initialize Page

//Include events file @1-8B5023FA
include_once("./activar_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-97922718
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$Panel14dias = new clsPanel("Panel14dias", $MainPage);
$Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", ccsGet, NULL), $MainPage);
$Link1->Page = "inicio.php";
$PanelPro = new clsPanel("PanelPro", $MainPage);
$Link2 = new clsControl(ccsLink, "Link2", "Link2", ccsText, "", CCGetRequestParam("Link2", ccsGet, NULL), $MainPage);
$Link2->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
$Link2->Page = "inicio.php";
$PanelAnual = new clsPanel("PanelAnual", $MainPage);
$Link3 = new clsControl(ccsLink, "Link3", "Link3", ccsText, "", CCGetRequestParam("Link3", ccsGet, NULL), $MainPage);
$Link3->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
$Link3->Page = "inicio.php";
$MainPage->Panel14dias = & $Panel14dias;
$MainPage->Link1 = & $Link1;
$MainPage->PanelPro = & $PanelPro;
$MainPage->Link2 = & $Link2;
$MainPage->PanelAnual = & $PanelAnual;
$MainPage->Link3 = & $Link3;
$Panel14dias->Visible = false;
$Panel14dias->AddComponent("Link1", $Link1);
$PanelPro->Visible = false;
$PanelPro->AddComponent("Link2", $Link2);
$PanelAnual->Visible = false;
$PanelAnual->AddComponent("Link3", $Link3);
$Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
$Link1->Parameters = CCAddParam($Link1->Parameters, "cuentanueva", CuentaNueva());

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

if ($Charset) {
    header("Content-Type: " . $ContentType . "; charset=" . $Charset);
} else {
    header("Content-Type: " . $ContentType);
}
//End Initialize Objects

//Initialize HTML Template @1-772A09F8
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "ISO-8859-1");
$Tpl->block_path = "/$BlockToParse";
$Attributes->SetValue("login", CCGetSession("_amember_login", NULL));
$Attributes->SetValue("pass", CCGetSession("_amember_pass", NULL));
$Attributes->SetValue("correo", CCGetSession("correo", NULL));
$Attributes->SetValue("nombre", CCGetSession("nombre", NULL));
$Attributes->SetValue("apellido", CCGetSession("apellido", NULL));
$Attributes->SetValue("lista", CCGetSession("lista", NULL));
$Attributes->SetValue("_amember_id", CCGetSession("_amember_id", NULL));
$Attributes->SetValue("cuentanueva", CuentaNueva());
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->SetValue("pathToRoot", "");
$Attributes->Show();
//End Initialize HTML Template


//Go to destination page @1-FBA93089
if($Redirect)
{
   #
   echo $redirect;
   exit;
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    header("Location: " . $Redirect);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-0C05BE24
$Panel14dias->Show();
$PanelPro->Show();
$PanelAnual->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
if (!isset($main_block)) $main_block = $Tpl->GetVar($BlockToParse);
$main_block = CCConvertEncoding($main_block, $FileEncoding, $TemplateEncoding);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-74A7C1E7
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
unset($Tpl);
//End Unload Page

?>
